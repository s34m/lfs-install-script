#!/usr/bin/env bash
set -euo pipefail

export LFS=/mnt/lfs
cd /mnt/lfs/sources


chmod +x install_*

mkdir -pv $LFS/{bin,etc,lib,sbin,usr,var}
case $(uname -m) in
  x86_64) mkdir -pv $LFS/lib64 ;;
esac

mkdir -pv $LFS/tools

groupadd -f lfs
useradd -s /bin/bash -g lfs -m -k /dev/null lfs

passwd lfs


chown lfs $LFS
case $(uname -m) in
  x86_64) chown -v lfs $LFS/lib64 ;;
esac

chown -v lfs $LFS/sources

su - lfs
