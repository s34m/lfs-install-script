#!/usr/bin/env python3
#
import time
import os
import stdiomask
import tarfile
import wget
import pexpect
import sys
import curses
import shutil



def isThisCorrect():
    if input("Y/N?").lower() == "y":
        return True
    else:
        return False

LFS = "/mnt/lfs"
owd = os.getcwd()

print("Have you already mounted required partitions?")
if not isThisCorrect():
    # LFS Partition
    while True:
        lfsPartition = input("Input the LFS-Partition: ")
        print("The LFS-Partition is " + lfsPartition)
        if isThisCorrect():
            break

    print("Enter the File system of the LFS Partition " + lfsPartition)
    print("Possible Values are ext4, xfs, btrfs")
    while True:
        fsPartition = input("Enter the file system:")
        print("The File System is " + fsPartition)
        if isThisCorrect():
            break

    # Home Partition
    print("Do you have a seperat HOME partition?")
    hasHomePartition = isThisCorrect()
    if hasHomePartition:
        while True:
            homePartition = input("Input the HOME-Partition: ")
            print("The Home-Partition is " + homePartition)
            if isThisCorrect():
                break

        print("Enter the File system of the HOME Partition " + homePartition)
        print("Possible Values are ext4, xfs, btrfs")
        while True:
            fsHomePartition = input("Enter the file system:")
            print("The File System is " + fsHomePartition)
            if isThisCorrect():
                break

    ##Swap Partition
    print("Do you have a SWAP Partition?")
    hasSwapPartition = isThisCorrect()
    if hasSwapPartition:
        while True:
            swapPartition = input("Input the SWAP-Partition: ")
            print("The SWAP-Partition is " + swapPartition)
            if isThisCorrect():
                break

    os.system("mkdir -p " + LFS)
    os.system("mount -v -t " + fsPartition + " " + lfsPartition + " /mnt/lfs")
    os.system("mkdir -p " + LFS + "/sources")
    os.system("mkdir -p " + LFS + "/home")
    os.system("mkdir -p " + LFS + "/usr")

    os.system("chmod -v a+wt /mnt/lfs/sources")
    if hasHomePartition:
        os.system("mount -v -t " + fsHomePartition + " " + homePartition + " /mnt/lfs/home")

print("Are your files already downloaded?")
downloaded = isThisCorrect()
if not downloaded:
    os.chdir(LFS + "/sources")
    print("Downloading Source List")
    os.system("wget http://www.linuxfromscratch.org/lfs/view/stable-systemd/wget-list")
    print("Downloading Sources")
    time.sleep(2)
    os.system("wget --input-file=wget-list --continue --directory-prefix=/mnt/lfs/sources")
    os.system("wget --directory-prefix=/mnt/lfs/sources https://ftp.gnu.org/gnu/stow/stow-latest.tar.gz")

    os.system("rm -rf linux-*")

    # Prepare Linux Kernel
    kernelDownload = input("Please enter the download link to the kernel you want to use: ")
    os.system("wget --directory-prefix=/mnt/lfs/sources " + kernelDownload)
    

os.chdir(owd)

os.system("cp install_* /mnt/lfs/sources")






